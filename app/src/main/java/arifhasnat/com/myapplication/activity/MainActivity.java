package arifhasnat.com.myapplication.activity;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.NavigationView;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.TextView;

import java.util.ArrayList;

import arifhasnat.com.myapplication.R;
import arifhasnat.com.myapplication.adapters.UpCommingAdapter;
import arifhasnat.com.myapplication.model.UpCommingModel;

public class MainActivity extends AppCompatActivity {


    private RecyclerView upCommingRecycler;
    private UpCommingAdapter upCommingAdapter;
    private RecyclerView.LayoutManager upCommingLayoutManager;


//    Toolbar toolbar;
//
//    private ViewPager mViewPager;
//    private TabLayout mTabLayout;
//    private int[] tabIcons = {
//            R.drawable.ic_home_black_24dp,
//            R.drawable.ic_home_black_24dp,
//            R.drawable.ic_home_black_24dp,
//            R.drawable.ic_home_black_24dp
//    };

    private NavigationView navigationView;


    private TextView mTextMessage;

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_home:
                    mTextMessage.setText("Home");
                    return true;
                case R.id.navigation_dashboard:
                    mTextMessage.setText("Booking");
                    return true;
                case R.id.inbox:
                    mTextMessage.setText("Inbox");
                    return true;
                case R.id.saved:
                    mTextMessage.setText("Saved");
                    return true;
                case R.id.settings:
                    mTextMessage.setText("Settings");
                    return true;
            }
            return false;
        }

    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);




//        mViewPager = (ViewPager) findViewById(R.id.viewpager);
//        setupViewPager(mViewPager);
//        mTabLayout = (TabLayout) findViewById(R.id.tabs);
//        mTabLayout.setupWithViewPager(mViewPager);
//     //   setupTabIcons();
//
//
//        mViewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
//
//            @Override
//            public void onPageSelected(int position) {
//                switch (position) {
//                    case 0:
//                        getSupportActionBar().setTitle("Upcomming");
//                        break;
//                    case 1:
//                        getSupportActionBar().setTitle("Requests");
//
//                        break;
//                    case 2:
//                        getSupportActionBar().setTitle("Completed");
//                        break;
//                    case 3:
//                        getSupportActionBar().setTitle("Completed");
//                        break;
//                    default:
//                        getSupportActionBar().setTitle("Completed");
//                        break;
//                }
//
//            }
//
//            @Override
//            public void onPageScrolled(int position, float offset, int offsetPixel) {
//            }
//
//            @Override
//            public void onPageScrollStateChanged(int state) {
//
//            }
//        });





        upCommingRecycler = (RecyclerView) findViewById(R.id.upcomming_recycler);
        upCommingRecycler.setHasFixedSize(true);
        upCommingLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        upCommingRecycler.setLayoutManager(upCommingLayoutManager);

         /* add pagination layout */

        ArrayList<UpCommingModel> upCommingModelArrayList= new ArrayList<UpCommingModel>();

        upCommingModelArrayList.add(new UpCommingModel("Charles Schneider ","28 Feb, 2017 02:00 PM - 04:55 PM"," Total Paid: $39 ","Starts in : 60 min"));
        upCommingModelArrayList.add(new UpCommingModel("Charles Schneider ","28 Feb, 2017 02:00 PM - 04:55 PM"," Total Paid: $39 ","Starts in : 60 min"));
        upCommingModelArrayList.add(new UpCommingModel("Charles Schneider ","28 Feb, 2017 02:00 PM - 04:55 PM"," Total Paid: $39 ","Starts in : 60 min"));
        upCommingModelArrayList.add(new UpCommingModel("Charles Schneider ","28 Feb, 2017 02:00 PM - 04:55 PM"," Total Paid: $39 ","Starts in : 60 min"));
        upCommingModelArrayList.add(new UpCommingModel("Charles Schneider ","28 Feb, 2017 02:00 PM - 04:55 PM"," Total Paid: $39 ","Starts in : 60 min"));
        upCommingModelArrayList.add(new UpCommingModel("Charles Schneider ","28 Feb, 2017 02:00 PM - 04:55 PM"," Total Paid: $39 ","Starts in : 60 min"));
        upCommingModelArrayList.add(new UpCommingModel("Charles Schneider ","28 Feb, 2017 02:00 PM - 04:55 PM"," Total Paid: $39 ","Starts in : 60 min"));
        upCommingModelArrayList.add(new UpCommingModel("Charles Schneider ","28 Feb, 2017 02:00 PM - 04:55 PM"," Total Paid: $39 ","Starts in : 60 min"));
        upCommingModelArrayList.add(new UpCommingModel("Charles Schneider ","28 Feb, 2017 02:00 PM - 04:55 PM"," Total Paid: $39 ","Starts in : 60 min"));
        upCommingModelArrayList.add(new UpCommingModel("Charles Schneider ","28 Feb, 2017 02:00 PM - 04:55 PM"," Total Paid: $39 ","Starts in : 60 min"));
        upCommingModelArrayList.add(new UpCommingModel("Charles Schneider ","28 Feb, 2017 02:00 PM - 04:55 PM"," Total Paid: $39 ","Starts in : 60 min"));


        upCommingAdapter = new UpCommingAdapter(upCommingModelArrayList);
        upCommingRecycler.setAdapter(upCommingAdapter);




    }


}
