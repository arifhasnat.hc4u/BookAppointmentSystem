package arifhasnat.com.myapplication.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import java.util.ArrayList;
import arifhasnat.com.myapplication.R;
import arifhasnat.com.myapplication.model.UpCommingModel;

/**
 * Created by HC4U on 1/8/2017.
 */

public class UpCommingAdapter extends RecyclerView.Adapter<UpCommingAdapter.BlogHolder>  {



     private Context mContext;
     private ArrayList<UpCommingModel> upCommingModelArrayList;

    public UpCommingAdapter(ArrayList<UpCommingModel> upCommingModelArrayList) {
        this.upCommingModelArrayList = upCommingModelArrayList;
    }



    @Override
    public BlogHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        mContext = parent.getContext();

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.upcomming_adapter, parent, false);
        BlogHolder viewHolder = new BlogHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(BlogHolder holder, final int position) {



        holder.title.setText(upCommingModelArrayList.get(position).getTitle());
        holder.date.setText(upCommingModelArrayList.get(position).getDate());
        holder.total.setText(upCommingModelArrayList.get(position).getAmount());
        holder.start.setText(upCommingModelArrayList.get(position).getStarts());


//        holder.title.setText( "Charles Schneider ");
//        holder.date.setText("28 Feb, 2017 02:00 PM - 04:55 PM");
//        holder.total.setText(" Total Paid: $39 ");
//        holder.start.setText("Starts in : 60 min");






    }

    @Override
    public int getItemCount() {
        return upCommingModelArrayList.size();
    }

    public class BlogHolder extends RecyclerView.ViewHolder {
        public TextView title, date, total, start;


        public BlogHolder(View itemView) {
            super(itemView);

            title = (TextView) itemView.findViewById(R.id.name);
            date = (TextView) itemView.findViewById(R.id.date);
            total = (TextView) itemView.findViewById(R.id.total);
            start = (TextView) itemView.findViewById(R.id.starts);


        }
    }
}
