package arifhasnat.com.myapplication.model;

/**
 * Created by Arif Hasnat on 6/25/2017.
 */

public class UpCommingModel {
    private String title;
    private String date;
    private String amount;
    private String starts;

    public UpCommingModel(String title, String date, String amount, String starts) {
        this.title = title;
        this.date = date;
        this.amount = amount;
        this.starts = starts;
    }


    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getStarts() {
        return starts;
    }

    public void setStarts(String starts) {
        this.starts = starts;
    }
}
